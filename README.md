# Mountebank
mountebank is the first open source tool to provide cross-platform, multi-protocol test doubles over the wire. Simply point your application under test to mountebank instead of the real dependency, and test like you would with traditional stubs and mocks.

mountebank is the most capable open source service virtualization tool in existence, and will cure what ails you, guaranteed.

## How to test
1. First of all start Mountebank server, goes to docker directory `src/test/resources/docker` and run docker-compose `docker-compose up`
2. Start Postman and import collection from `src/test/resources/postman`
3. Run tests from collection

## 01-configuration
Demo to show initial setup of mountebank by docker container.

### Generate docker image
```
cd src/test/resources/docker
docker build --tag mountebank:0.0.1 .
```

### Tag docker image
```
docker tag mountebank:0.0.1 nexus.<url-repositorio>/mountebank:latest
```

### Push docker image
```
docker push mountebank:0.0.1 nexus.<url-repositorio>/mountebank:latest
```

### Run docker container
```
cd src/test/resources/docker
docker-compose up
```

## 02-predicates
It is important to note that once we start our container with the Mountebank server via docker we will be able to see the Mountbank output on the console

Below you can see an example of the standard output of a request made on the Mountebank server, in it you can see the stub chosen by Mountebank in the log `using predicate match`

```
autentia-it-mountebank_1  | info: [http:80 origin] ::ffff:172.30.0.1:39298 => POST /courses
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.30.0.1:39298 => {"requestFrom":"::ffff:172.30.0.1:39298","method":"POST","path":"/courses","query":{},"headers":{"Content-Type":"application/json","User-Agent":"PostmanRuntime/7.25.0","Accept":"*/*","Postman-Token":"b21afb37-137f-402c-8484-53cace61741b","Host":"localhost:80","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Length":"157"},"body":"{\n    \"id\": \"d355ef6e-1f12-4731-9ae7-64a863aca822\",\n    \"name\": \"Primeros pasos con github: subir un proyecto al repositorio.\",\n    \"level\": \"INTERMEDIATE\"\n}","ip":"::ffff:172.30.0.1"}
autentia-it-mountebank_1  | debug: [http:80 origin] using predicate match: [{"startsWith":{"method":"POST","path":"/courses","body":{"name":"Primeros pasos"}}}]
autentia-it-mountebank_1  | debug: [http:80 origin] generating response from {"is":{"statusCode":200}}
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.30.0.1:39298 <= {"statusCode":200,"headers":{"Connection":"close"},"body":"","_mode":"text"}
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.30.0.1:39294 CLOSED
```

### equals
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
      "predicates": [
        {
          "equals": {
            "method": "POST",
            "path": "/courses",
            "headers": {
              "Content-Type": "application/json"
            }
          }
        },
        {
          "equals": { 
              "body": {
                "level": "BASIC"
              } 
          },
          "caseSensitive": true
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 200
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "equals": {
            "method": "POST",
            "path": "/courses",
            "headers": {
              "Content-Type": "application/xml"
            }
          }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 406
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "equals": {
            "method": "PUT",
            "path": "/courses",
            "headers": {
              "Content-Type": "application/xml"
            }

          }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 405
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "equals": {
            "method": "PUT",
            "path": "/courses",
            "headers": {
              "Content-Type": "application/xml"
            }
          }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 500
          }
        }
      ]
    },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "c1e3ea3d-4ff1-4688-af52-60aff11401b1",
    "name": "Utilizar Docker en Windows",
    "level": "BASIC"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

Example of a POST request to match the second example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/xml

<course>
    <id>c1e3ea3d-4ff1-4688-af52-60aff11401b1</id>
    <name>Utilizar Docker en Windows</name>
    <level>BASIC</level>
</course>
```
Result
```
HTTP/1.1 406 Not Acceptable
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

Example of a PUT request to match the third example of the predicate
```
PUT /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "c1e3ea3d-4ff1-4688-af52-60aff11401b1",
    "name": "Utilizar Docker en Windows",
    "level": "BASIC"
}
```
Result
```
HTTP/1.1 405 Method Not Allowed
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

The fourth stub will never run, since it matches the same requests as the third stub. mountebank always chooses the first stub that matches based on the order you add them to the stubs array when creating the imposter.

### deepEquals
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
      "predicates": [
        {
          "deepEquals": {
            "method": "GET",
            "path": "/courses",
            "query": {
              "page": 1,
              "limit": 10
            }
          }
        }
      ],
      "responses": [
      {
        "is": {
          "statusCode" : 200,
          "headers":{
              "Content-Type":"application/json"
          },
          "body": [{
            "id": "fb68d450-3038-4bd5-9854-9cfba4dc5fb5",
            "name": "Ejb3 Timer Service: Scheduling",
            "level": "INTERMEDIATE"
          }]
        }
      }
      ]
    },
    {
      "predicates": [
        {
          "deepEquals": {
            "method": "GET",
            "path": "/courses",
            "query": {
              "page": 2,
              "limit": 10
            }
          }
        }
      ],
      "responses": [
      {
        "is": {
          "statusCode" : 200,
          "headers":{
              "Content-Type":"application/json"
          },
          "body": [{
            "id": "b2aa00b2-5fff-43c0-a9ca-172169b4fd5d",
            "name": "Envío de correo electrónico con el soporte de Jboss Seam",
            "level": "BASIC"
          }]
        }
      }
      ]
    },
    {
      "predicates": [
        {
          "deepEquals": {
            "method": "GET",
            "path": "/courses",
            "query": {
              "page": 3,
              "limit": 10
            }
          }
        }
      ],
      "responses": [
      {
        "is": {
          "statusCode" : 200,
          "headers":{
              "Content-Type":"application/json"
          },
          "body": [{
            "id": "13b88aee-1412-4290-b9a0-d6ea4ba5ca0f",
            "name": "Registro dinámico de beans en el contexto de Springframework",
            "level": "ADVANCED"
          }]
        }
      }
      ]
    },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}    
```
Example of a GET request to match the first example of the predicate
```
GET /courses?page=1&limit=10 HTTP/1.1
Host: localhost:80
Content-Type: application/json
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked

[{
    "id": "fb68d450-3038-4bd5-9854-9cfba4dc5fb5",
    "name": "Ejb3 Timer Service: Scheduling",
    "level": "INTERMEDIATE"
}]
```
Example of a GET request to match the second example of the predicate
```
GET /courses?page=2&limit=10 HTTP/1.1
Host: localhost:80
Content-Type: application/json
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked

[{
    "id": "b2aa00b2-5fff-43c0-a9ca-172169b4fd5d",
    "name": "Envío de correo electrónico con el soporte de Jboss Seam",
    "level": "BASIC"
}]
```
Example of a GET request to match the third example of the predicate
```
GET /courses?page=3&limit=10 HTTP/1.1
Host: localhost:80
Content-Type: application/json
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked

[{
    "id": "13b88aee-1412-4290-b9a0-d6ea4ba5ca0f",
    "name": "Registro dinámico de beans en el contexto de Springframework",
    "level": "ADVANCED"
}]
```
Any additional query parameters will trigger the default HTTP response.
```
GET /courses?page=1&limit=10&key=value HTTP/1.1
Host: localhost:80
Content-Type: application/json
```
Result
```
HTTP/1.1 404 Not found
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

### contains
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
      "predicates": [
        {
          "contains": {
                "body": {
                    "name": "Spring"
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 200
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "contains": {
                "body": {
                    "name": "Windows"
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 200
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "contains": {
                "body": {
                    "name": "Güindous"
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 500
          }
        }
      ]
    },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "13b88aee-1412-4290-b9a0-d6ea4ba5ca0f",
    "name": "Registro dinámico de beans en el contexto de Springframework",
    "level": "ADVANCED"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the second example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "c1e3ea3d-4ff1-4688-af52-60aff11401b1",
    "name": "Utilizar Docker en Windows",
    "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the third example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "c1e3ea3d-4ff1-4688-af52-60aff11401b1",
    "name": "Utilizar Docker en Güindous",
    "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 500 Internal server errror
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

### startsWith
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
      "predicates": [
        {
          "startsWith": {
                "method": "POST",
                "path": "/courses",
                "body": {
                    "name": "Primeros pasos"
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 202
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "startsWith": {
                "method": "POST",
                "path": "/courses",
                "body": {
                    "name": "Utilizar"
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 202
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "startsWith": {
                "method": "POST",
                "path": "/courses",
                "body": {
                    "name": "Utlsar"
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 502
          }
        }
      ]
    },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "d355ef6e-1f12-4731-9ae7-64a863aca822",
    "name": "Primeros pasos con github: subir un proyecto al repositorio.",
    "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the second example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "c1e3ea3d-4ff1-4688-af52-60aff11401b1",
    "name": "Utilizar Docker en Linux",
    "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the third example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "c1e3ea3d-4ff1-4688-af52-60aff11401b1",
    "name": "Utlsar Docker en Linux",
    "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 500 Internal server errror
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

### endsWith
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
      "predicates": [
        {
          "endsWith": {
                "method": "POST",
                "path": "/courses",
                "body": {
                    "name": "Scheduling."
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 200
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "endsWith": {
                "method": "POST",
                "path": "/courses",
                "body": {
                    "name": "Seam."
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 200
          }
        }
      ]
    },
    {
      "predicates": [
        {
            "endsWith": {
                "method": "POST",
                "path": "/courses",
                "body": {
                    "name": " ."
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 500
          }
        }
      ]
    },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
  "id": "fb68d450-3038-4bd5-9854-9cfba4dc5fb5",
  "name": "Ejb3 Timer Service: Scheduling.",
  "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the first second of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
  "id": "b2aa00b2-5fff-43c0-a9ca-172169b4fd5d",
  "name": "Envío de correo electrónico con el soporte de Jboss Seam.",
  "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the third example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
  "id": "b2aa00b2-5fff-43c0-a9ca-172169b4fd5d",
  "name": "Envío de correo electrónico con el soporte de Jboss Seam .",
  "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 500 Internal server errror
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

### matches
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
      "predicates": [
        {
          "matches": {
                "method": "POST",
                "path": "/courses",
                "body": {
                    "name": "^Spring Security\\Wembebido."
                } 
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 200
          }
        }
      ]
    },
    {
      "predicates": [
        {
          "matches": {
                "method": "PUT",
                "path": "/courses/(.*)"
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 200
          }
        }
      ]
    },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
  "id": "f8d918da-7eb7-4423-8662-b8bf1ffdb2d5",
  "name": "Spring Security: haciendo uso de un servidor LDAP embebido.",
  "level": "ADVANCED"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the first second of the predicate
```
POST /courses HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
  "id": "b2aa00b2-5fff-43c0-a9ca-172169b4fd5d",
  "name": "Envío de correo electrónico con el soporte de Jboss Seam.",
  "level": "INTERMEDIATE"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

### exists
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
      "predicates": [
        {
          "exists": {
              "query": {
                "page": true,
                "limit": true,
                "search": false
              },
              "headers": {
                "Content-Type": true,
                "X-Rate-Limit": false
              }
            }
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 200
          }
        }
      ]
    },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 200,
            "headers":{
                "Content-Type":"application/json"
            },
            "body": [{
              "id": "fb68d450-3038-4bd5-9854-9cfba4dc5fb5",
              "name": "Ejb3 Timer Service: Scheduling",
              "level": "INTERMEDIATE"
            }]
          }
        }
        ]
    }
  ]
}
```
Example of a GET request to match the first example of the predicate
```
GET /courses?page=1&limit=100 HTTP/1.1
Host: localhost:80
Content-Type: application/json
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked

[{
    "id": "fb68d450-3038-4bd5-9854-9cfba4dc5fb5",
    "name": "Ejb3 Timer Service: Scheduling",
    "level": "INTERMEDIATE"
}]
```

### not
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
      "predicates": [
        {
            "not": {
                "equals": {
                    "path": "/courses"
                }                    
            }          
        }
      ],
      "responses": [
        {
          "is": {
            "statusCode" : 500
          }
        }
      ]
    },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /cursos HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "c1e3ea3d-4ff1-4688-af52-60aff11401b1",
    "name": "Utilizar Docker en Windows",
    "level": "BASIC"
}
```
Result
```
HTTP/1.1 500 Internal server error
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

### or
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
       "predicates": [
         {
           "or": [
               { "startsWith": { "body": { "name": "Validación" } } },
               { "contains": { "body": { "name": "formularios" } } }
             ]
         }
       ],
       "responses": [
         {
           "is": {
             "statusCode" : 200
           }
         }
       ]
     },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /cursos HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "79b2181e-65b9-443e-9340-f382c59e9d52",
    "name": "Validación personalizada de formularios en Angular.",
    "level": "ADVANCED"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the first example of the predicate
```
POST /cursos HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "79b2181e-65b9-443e-9340-f382c59e9d52",
    "name": "Validación de formularios en AngularJS.",
    "level": "ADVANCED"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

### and
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
       "predicates": [
         {
           "and": [
               { "startsWith": { "body": { "name": "Refactorizando" } } },
               { "endsWith": { "body": { "name": "." } } },
               { "contains": { "body": { "name": "Test" } } }
             ]
         }
       ],
       "responses": [
         {
           "is": {
             "statusCode" : 200
           }
         }
       ]
     },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /cursos HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "9c6fc9ab-b52c-4a64-892b-51e81dcb2d81",
    "name": "Refactorizando métodos no-deterministas para poder hacer Test Unitarios.",
    "level": "ADVANCED"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```
Example of a POST request to match the first example of the predicate
```
POST /cursos HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "9c6fc9ab-b52c-4a64-892b-51e81dcb2d81",
    "name": "Refactorizando métodos no-deterministas para poder hacer Test Unitarios",
    "level": "ADVANCED"
}
```
Result
```
HTTP/1.1 404 Not found
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

### inject
Let's create an HTTP imposter with multiple stubs:
```
{
  "port": 80,
  "protocol": "http",
  "name": "origin",
  "defaultResponse": {
    "statusCode": 400
  },
  "stubs": [
    {
       "predicates": [
         {
           "inject": "function (config) {return config.request.path == '/courses' && config.request.method == 'POST' && JSON.parse(config.request.body).name == 'Monitorización y alertas en aplicaciones java con AppDynamic' && JSON.parse(config.request.body).level == 'ADVANCED'; }"
         }
       ],
       "responses": [
         {
           "is": {
             "statusCode" : 200
           }
         }
       ]
     },
    {
      "responses": [
        {
          "is": {
            "statusCode" : 404
          }
        }
      ]
    }
  ]
}
```
Example of a POST request to match the first example of the predicate
```
POST /cursos HTTP/1.1
Host: localhost:80
Content-Type: application/json

{
    "id": "3d6dbe92-5710-49c4-97bb-0a3f712e1d52",
    "name": "Monitorización y alertas en aplicaciones java con AppDynamic",
    "level": "ADVANCED"
}
```
Result
```
HTTP/1.1 200 Ok
Connection: close
Date: Thu, 09 Jun 2020 02:30:31 GMT
Transfer-Encoding: chunked
```

## 03-is-response

### How to test endless test
1. First of all start Mountebank server, goes to docker directory `src/test/resources/docker` and run docker-compose `docker-compose up`
2. Start Postman and import collection from `src/test/resources/postman`
3. Open endless test
4. Run and check result is 54
5. Run again without stopping and starting the container and check the result is 21
6. Run again without stopping and starting the container and check the result is 0

## 04-proxy-response
In the examples we will use the online api rest to test the following url:`http://jsonplaceholder.typicode.com/`

To better understand how proxy responses work it is a good practice once the request is executed to make a call to the Mountebank imposter point to see what behaves differently for each mode type 

### proxyOnce
To see how the "proxyOnce" mode works, let's look at the Mountebank console. After booting the console and launching the request for the first time, we see the next console output:
```
$ docker-compose up                   
Creating network "autentia" with driver "bridge"
Creating docker_autentia-it-mountebank_1 ... done
Attaching to docker_autentia-it-mountebank_1
autentia-it-mountebank_1  | warn: [mb:2525] Running with --allowInjection set. See http://localhost:2525/docs/security for security info
autentia-it-mountebank_1  | info: [mb:2525] mountebank v2.2.1 now taking orders - point your browser to http://localhost:2525/ for help
autentia-it-mountebank_1  | debug: [mb:2525] config: {"options":{"configfile":"/mountebank/servers/service.ejs","loglevel":"debug","allowInjection":true,"allow-injection":true,"port":2525,"noParse":false,"no-parse":false,"pidfile":"mb.pid","nologfile":false,"logfile":"mb.log","localOnly":false,"local-only":false,"ipWhitelist":["*"],"ip-whitelist":"*","mock":false,"debug":false,"heroku":false,"protofile":"protocols.json"},"process":{"nodeVersion":"v12.16.1","architecture":"x64","platform":"linux"}}
autentia-it-mountebank_1  | info: [mb:2525] PUT /imposters
autentia-it-mountebank_1  | debug: [mb:2525] ::ffff:127.0.0.1:34260 => {"imposters":[{"port":80,"protocol":"http","name":"origin","defaultResponse":{"statusCode":404},"stubs":[{"responses":[{"proxy":{"to":"http://jsonplaceholder.typicode.com","mode":"proxyOnce","predicateGenerators":[{"matches":{"path":true}}]}}]}]}]}
autentia-it-mountebank_1  | info: [http:80 origin] Open for business...
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35288 ESTABLISHED
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35292 ESTABLISHED
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35288 LAST-ACK
autentia-it-mountebank_1  | info: [http:80 origin] ::ffff:172.23.0.1:35292 => GET /comments
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35292 => {"requestFrom":"::ffff:172.23.0.1:35292","method":"GET","path":"/comments","query":{},"headers":{"Content-Type":"text/plain","User-Agent":"PostmanRuntime/7.25.0","Accept":"*/*","Postman-Token":"ccccd4ae-a3d8-4a94-ad4f-42b3cf5f605f","Host":"localhost:80","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive"},"body":"","ip":"::ffff:172.23.0.1"}
autentia-it-mountebank_1  | debug: [http:80 origin] using predicate match: {}
autentia-it-mountebank_1  | debug: [http:80 origin] generating response from {"proxy":{"to":"http://jsonplaceholder.typicode.com","mode":"proxyOnce","predicateGenerators":[{"matches":{"path":true}}]}}
autentia-it-mountebank_1  | debug: [http:80 origin] Proxy ::ffff:172.23.0.1:35292 => {"requestFrom":"::ffff:172.23.0.1:35292","method":"GET","path":"/comments","query":{},"headers":{"Content-Type":"text/plain","User-Agent":"PostmanRuntime/7.25.0","Accept":"*/*","Postman-Token":"ccccd4ae-a3d8-4a94-ad4f-42b3cf5f605f","Host":"localhost:80","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive"},"body":"","ip":"::ffff:172.23.0.1"} => http://jsonplaceholder.typicode.com
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35288 CLOSED
autentia-it-mountebank_1  | debug: [http:80 origin] Proxy ::ffff:172.23.0.1:35292 <= {"statusCode":200,"headers":{"Date":"Mon, 22 Jun 2020 16:29:57 GMT","Content-Type":"application/json; charset=utf-8","Transfer-Encoding":"chunked","Connection":"keep-alive","Set-Cookie":"__cfduid=d950770d7f2411248216ff4a8854b6cde1592843397; expires=Wed, 22-Jul-20 16:29:57 GMT; path=/; domain=.typicode.com; HttpOnly; SameSite=Lax","X-Powered-By":"Express","Vary":"Origin, Accept-Encoding","Access-Control-Allow-Credentials":"true","Cache-Control":"max-age=43200","Pragma":"no-cache","Expires":"-1","X-Content-Type-Options":"nosniff","Etag":"W/\"26831-8L/dCHuriTuxj+1OR5biYTSa5Yo\"","Content-Encoding":"gzip","Via":"1.1 vegur","CF-Cache-Status":"HIT","Age":"23628","cf-request-id":"037e7683460000c66b5594e200000001","Server":"cloudflare","CF-RAY":"5a7759e53814c66b-MAD"},"body":"H4sIA...==","_mode":"binary"} <= http://jsonplaceholder.typicode.com
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35292 <= {"statusCode":200,"headers":{"Date":"Mon, 22 Jun 2020 16:29:57 GMT","Content-Type":"application/json; charset=utf-8","Transfer-Encoding":"chunked","Connection":"keep-alive","Set-Cookie":"__cfduid=d950770d7f2411248216ff4a8854b6cde1592843397; expires=Wed, 22-Jul-20 16:29:57 GMT; path=/; domain=.typicode.com; HttpOnly; SameSite=Lax","X-Powered-By":"Express","Vary":"Origin, Accept-Encoding","Access-Control-Allow-Credentials":"true","Cache-Control":"max-age=43200","Pragma":"no-cache","Expires":"-1","X-Content-Type-Options":"nosniff","Etag":"W/\"26831-8L/dCHuriTuxj+1OR5biYTSa5Yo\"","Content-Encoding":"gzip","Via":"1.1 vegur","CF-Cache-Status":"HIT","Age":"23628","cf-request-id":"037e7683460000c66b5594e200000001","Server":"cloudflare","CF-RAY":"5a7759e53814c66b-MAD"},"body":"H4sIAA...==","_mode":"binary"}
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35292 CLOSED
```
We can observe how Mountebank detects that it is the first time that it is called to that endpoint and therefore it makes the call to `jsonplaceholder`, later we see how Mountebank stores the answer and as we will see in the next fragment of the console, in later calls that we make over the endpoint it will always return us the stored answer:
```
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35310 ESTABLISHED
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35314 ESTABLISHED
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35310 LAST-ACK
autentia-it-mountebank_1  | info: [http:80 origin] ::ffff:172.23.0.1:35314 => GET /comments
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35314 => {"requestFrom":"::ffff:172.23.0.1:35314","method":"GET","path":"/comments","query":{},"headers":{"Content-Type":"text/plain","User-Agent":"PostmanRuntime/7.25.0","Accept":"*/*","Postman-Token":"711bc446-56de-4378-b8c8-d85b707d8bd5","Host":"localhost:80","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive"},"body":"","ip":"::ffff:172.23.0.1"}
autentia-it-mountebank_1  | debug: [http:80 origin] using predicate match: [{"deepEquals":{"path":"/comments"}}]
autentia-it-mountebank_1  | debug: [http:80 origin] generating response from {"is":{"statusCode":200,"headers":{"Date":"Mon, 22 Jun 2020 16:29:57 GMT","Content-Type":"application/json; charset=utf-8","Transfer-Encoding":"chunked","Connection":"keep-alive","Set-Cookie":"__cfduid=d950770d7f2411248216ff4a8854b6cde1592843397; expires=Wed, 22-Jul-20 16:29:57 GMT; path=/; domain=.typicode.com; HttpOnly; SameSite=Lax","X-Powered-By":"Express","Vary":"Origin, Accept-Encoding","Access-Control-Allow-Credentials":"true","Cache-Control":"max-age=43200","Pragma":"no-cache","Expires":"-1","X-Content-Type-Options":"nosniff","Etag":"W/\"26831-8L/dCHuriTuxj+1OR5biYTSa5Yo\"","Content-Encoding":"gzip","Via":"1.1 vegur","CF-Cache-Status":"HIT","Age":"23628","cf-request-id":"037e7683460000c66b5594e200000001","Server":"cloudflare","CF-RAY":"5a7759e53814c66b-MAD"},"body":"H4sIA...==","_mode":"binary","_proxyResponseTime":67}}
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35314 <= {"statusCode":200,"headers":{"Date":"Mon, 22 Jun 2020 16:29:57 GMT","Content-Type":"application/json; charset=utf-8","Transfer-Encoding":"chunked","Connection":"keep-alive","Set-Cookie":"__cfduid=d950770d7f2411248216ff4a8854b6cde1592843397; expires=Wed, 22-Jul-20 16:29:57 GMT; path=/; domain=.typicode.com; HttpOnly; SameSite=Lax","X-Powered-By":"Express","Vary":"Origin, Accept-Encoding","Access-Control-Allow-Credentials":"true","Cache-Control":"max-age=43200","Pragma":"no-cache","Expires":"-1","X-Content-Type-Options":"nosniff","Etag":"W/\"26831-8L/dCHuriTuxj+1OR5biYTSa5Yo\"","Content-Encoding":"gzip","Via":"1.1 vegur","CF-Cache-Status":"HIT","Age":"23628","cf-request-id":"037e7683460000c66b5594e200000001","Server":"cloudflare","CF-RAY":"5a7759e53814c66b-MAD"},"body":"H4sIAA...==","_mode":"binary"}
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35310 CLOSED
autentia-it-mountebank_1  | debug: [http:80 origin] ::ffff:172.23.0.1:35314 CLOSED
```

### proxyAlways
However, if we use proxyAlways mode we can see how all the calls we make on the Mountebank endpoint will go to the real endpoint

## 05-inject-response
In the examples the stringify function has been used to make the templates more readable.

Within the Postman test collection, a series of tests have been left to test add status functionality between requests provided by Mountebank

## 06-behaviors

### copy
To run this test correctly, you have to launch at least 4 times the endpoint


